// General purpose crates
#[macro_use]
extern crate clap;
#[macro_use]
extern crate log;
extern crate env_logger;

// GTK crates
extern crate gtk;

// Relm
#[macro_use]
extern crate relm;
#[macro_use]
extern crate relm_derive;
#[macro_use]
extern crate relm_attributes;

// GFX crates
extern crate gfx_backend_vulkan as vk;
extern crate gfx_hal as gfx;
extern crate winit;

mod builder;

fn main() {
    let yaml = load_yaml!("appinfo.yml");
    let _matches = clap::App::from_yaml(yaml).get_matches();
    env_logger::init_from_env("LOG_LEVEL");

    use relm::Widget;
    builder::Win::run(()).unwrap();
}

