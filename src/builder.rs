use gtk::{
    OrientableExt,
    WidgetExt,
    ComboBoxExt,
    ComboBoxTextExt,
    GtkListStoreExtManual,
    StaticType
};

pub struct Model {
    rpass_pool: Vec<u32>,
    list_store: gtk::ListStore,
}

#[derive(Msg)]
pub enum Msg {
    Clicked,
    Quit,
}

#[widget]
impl relm::Widget for Win {
    fn model(relm: &relm::Relm<Self>, _: ()) -> Model {
        Model {
            rpass_pool: Vec::new(),
            list_store: create_model()
        }
    }

    // Update the model according to the message received.
    fn update(&mut self, event: Msg) {
        match event {
            Msg::Clicked => trace!("Clicked"),
            Msg::Quit => gtk::main_quit(),
        }
    }

    view! {
        gtk::Window {
            gtk::Box {
                orientation: gtk::Orientation::Vertical,
                gtk::ComboBoxText {
                    changed(entry) => {
                        trace!("Changed {:?}", entry.get_active_text());
                        Msg::Clicked
                    },
                    title: "Test",
                    model: &self.model.list_store,
                    entry_text_column: 0,
                    id_column: 1,
                },
            },
            delete_event(_, _) => (Msg::Quit, gtk::Inhibit(false)),
        }
    }
}

fn create_model() -> gtk::ListStore {
    let model = gtk::ListStore::new(&[String::static_type(), u32::static_type()]);

    for i in 0..10 {
        model.insert_with_values(None,
                                 &[0u32, 1u32],
                                 &[&"Test", &i]);
    }

    model
}
