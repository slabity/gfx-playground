let
pkgs = import <nixpkgs> {};
in with pkgs; stdenv.mkDerivation rec {
  name = "vulkan-env";

  nativeBuildInputs = [
    cmake
    pkgconfig
    python3
  ];

  buildInputs = with xlibs; [
    libxcb.dev
    libX11.dev
    gtk3.dev
  ];

  LD_LIBRARY_PATH = with xlibs; stdenv.lib.makeLibraryPath [
    libX11
    libXcursor
    libXrandr
    libXi
    vulkan-loader
  ];
}
